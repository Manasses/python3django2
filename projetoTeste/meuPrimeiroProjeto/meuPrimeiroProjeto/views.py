from django.http import HttpResponse
from django.shortcuts import render

def hello(request):
    #return HttpResponse('Ola mundo')
    return render(request, 'index.html')

def articles(request, year):
    return HttpResponse('O ano enviado foi: ' + str(year))

def lerDoBanco(nome):
    lista_nomes = [
        {'nome': 'Ana', 'idade': 20},
        {'nome': 'Pedro', 'idade': 25},
        {'nome': 'Joaquim', 'idade': 27},
    ]

    for pessoa in lista_nomes:
        if pessoa['nome'] == nome:
            return pessoa
    return {'nome': 'Não encontrado.', 'idade': 0}


def fname(request, nome):
    result = lerDoBanco(nome)
    return HttpResponse(result['nome'] + ' - ' + str(result['idade']))

def fname2(request, nome):
    idade = lerDoBanco(nome)['idade']
    return render(request, 'pessoa.html', {'v_idade': idade})